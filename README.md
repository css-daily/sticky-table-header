# Sticky table header

Usually using position:sticky on th elements of a table header should be enough
to make those elements sticky.

This library is just for the case where one wants to create the sticky effect
where the page scroll is used for vertical scrolling of a table but a container
element overflow scroll is used for horizontal scrolling.
