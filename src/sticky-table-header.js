(function() {
	function stickyTableHeaderWithFixed() {
		// Polyfill NodeList.forEach for IE11.
		if (window.NodeList && !NodeList.prototype.forEach) {
			NodeList.prototype.forEach = Array.prototype.forEach;
		}

		function stickTableHeader(tableElm) {
			const thead = tableElm.querySelector('thead');
			const tbody = tableElm.querySelector('tbody');
			const lastHeaderRow = thead.querySelector('tr:last-child');
			const headerRowCols = lastHeaderRow.querySelectorAll('th');
			const firstBodyRow = tbody.querySelector('tr:first-child');
			const firstRowCols = firstBodyRow.querySelectorAll('td');
			const tableContainer = tableElm.parentElement;
			// For after the connection between the table thead and tbody is separated,
			// set the cell widths, so that the column sizes remain the same.
			headerRowCols.forEach(function(headCol, index) {
				const bodyCol = firstRowCols[index];
				const bodyColWidth = bodyCol.getBoundingClientRect().width;
				const headColWidth = headCol.getBoundingClientRect().width;
				const maxColWidth = Math.ceil(Math.max(bodyColWidth, headColWidth)) + 'px';
				headCol.style.width = maxColWidth;
				headCol.style.minWidth = maxColWidth;
				bodyCol.style.width = maxColWidth;
				bodyCol.style.minWidth = maxColWidth;
			});
			// Ensure tbody and thead have the same width.
			tbody.style.width = thead.getBoundingClientRect().width + 'px';
			tbody.style.display = 'block';
			// Fix the position of the thead relative to the viewport.
			thead.style.position = 'fixed';
			thead.style.width = tableElm.parentElement.getBoundingClientRect().width + 'px';
			// Scrolling will be synced with the body scroll offset, so hidden can
			// be used here.
			thead.style.overflow = 'hidden';
			thead.style.display = 'block';
			thead.style.zIndex = 2;
			// Make space for the head to set at the top after it is removed from
			// the table flow.
			tableElm.style.paddingTop = thead.getBoundingClientRect().height + 'px';
			thead.style.top = tableElm.getBoundingClientRect().top + 'px';

			// Function to be used as the page scrolls vertically to enable the sticky
			// effect.
			function doStickyEffect() {
				const tableRect = tableElm.getBoundingClientRect();
				const theadRect = thead.getBoundingClientRect();
				const bottomOffset = tableRect.height + tableRect.top - theadRect.height;
				if (bottomOffset <= 0) {
					thead.style.top = bottomOffset + 'px';
				} else {
					thead.style.top = Math.max(0, tableRect.top) + 'px';
				}
			}
			window.addEventListener('scroll', doStickyEffect);
			doStickyEffect();

			// Function to be used as the user horizontally scrolls the table so that
			// the header and body columns remain in sync.
			lastHeaderRow.style.position = 'relative';
			lastHeaderRow.style.display = 'block';
			const initialOffset = tbody.getBoundingClientRect().left + tableContainer.scrollLeft;
			function syncScrolls() {
				lastHeaderRow.style.left = (tbody.getBoundingClientRect().left - initialOffset) + 'px';
			}
			tableContainer.addEventListener('scroll', syncScrolls);
			syncScrolls();

			// On window resize effects, throw everything away and start again from
			// scratch.
			function handleResize() {
				window.removeEventListener('resize', handleResize);
				window.removeEventListener('scroll', doStickyEffect);
				tableContainer.removeEventListener('scroll', syncScrolls);
				headerRowCols.forEach(function(headCol, index) {
					const bodyCol = firstRowCols[index];
					headCol.style.width = '';
					headCol.style.minWidth = '';
					bodyCol.style.width = '';
					bodyCol.style.minWidth = '';
				})
				tbody.style.width = '';
				tbody.style.display = '';
				thead.style.position = '';
				thead.style.width = '';
				thead.style.overflow = '';
				thead.style.display = '';
				thead.style.zIndex = '';
				tableElm.style.paddingTop = ''; 
				thead.style.top = '';
				lastHeaderRow.style.position = '';
				lastHeaderRow.style.display = '';
				lastHeaderRow.style.left = '';
				// Throttle the resize handlers.
				setTimeout(function() {
					stickTableHeader(tableElm);
					isInResetState = false;
				}, 250);
			}
			window.addEventListener('resize', handleResize);
		}
		const tables = document.querySelectorAll('table.sticky-table-header');
		tables.forEach(function(table) {
			stickTableHeader(table);
		});

	}

	function stickyTableHeaderWithTransform() {
		function stickTableHeader(tableElm) {
			const thead = tableElm.querySelector('thead');
			const theadHeight = Math.ceil(thead.getBoundingClientRect().height);
			const tableHeight = tableElm.getBoundingClientRect().height;
	
			// Function to be used as the page scrolls vertically to enable the sticky
			// effect.
			function doStickyEffect() {
				const tableRect = tableElm.getBoundingClientRect();
				const tableTop = tableRect.top;
				if (tableTop <= 0) {
					let yTranslate = Math.floor(-1 * tableTop);
					const bottomOffset = Math.floor(tableHeight + tableTop - theadHeight - 1);
					if (bottomOffset <= 0) {
						yTranslate += bottomOffset;
					}
					thead.style.transform = 'translateY(' + yTranslate + 'px)';
				}
				else {
					thead.style.transform = 'translateY(0)';
				}
			}

			window.addEventListener('scroll', function() {
				window.requestAnimationFrame(doStickyEffect);
			});
			doStickyEffect();

			function handleResize() {
				window.requestAnimationFrame(doStickyEffect);
			}
			window.addEventListener('resize', handleResize);
		}

		const tables = document.querySelectorAll('table.sticky-table-header');
		tables.forEach(function(table) {
			stickTableHeader(table);
		});
	}

	// If IE, use fixed positioning (transform does not work on thead)
	if (window.document.documentMode) {
		stickyTableHeaderWithFixed();
	}
	else {
		// Otherwise, use transform positioning.
		stickyTableHeaderWithTransform();
	}
})();
